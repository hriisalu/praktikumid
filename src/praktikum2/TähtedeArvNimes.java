package praktikum2;

import library.TextIO;

public class TähtedeArvNimes {

	public static void main(String[] args) {

		System.out.println("Mis on su nimi?");
		String nimi = TextIO.getln();
		String nimiAinultT2hed = nimi.replaceAll(" ","").replaceAll("-","");
		int nimePikkus = nimiAinultT2hed.length();
		System.out.println("Su nimes on " + nimePikkus + " tähte.");

	}

}
