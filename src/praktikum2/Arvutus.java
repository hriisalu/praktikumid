package praktikum2;

import library.TextIO;

public class Arvutus {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
//defineerin muutujad		
		int arv1;
		int arv2;
		
		System.out.println("Palun sisesta kaks arvu.");

//väärtustan muutujad
		arv1 = TextIO.getlnInt();
		arv2 = TextIO.getlnInt();
		
	//korrutame väärtused
		int korrutis = arv1 * arv2;
		
		System.out.println("Arvude korrutis on: " + korrutis);
	}

}
