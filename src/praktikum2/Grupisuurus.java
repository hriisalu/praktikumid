package praktikum2;

import library.TextIO;

public class Grupisuurus {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Palun sisesta inimeste arv.");
		int inimesteArv = TextIO.getlnInt();
		System.out.println("Palun sisesta grupi suurus.");
		int grupiSuurus = TextIO.getlnInt();

		int gruppideArv = inimesteArv / grupiSuurus;
		System.out.println("Saame moostustada " + gruppideArv + " gruppi.");

		// inimeste ülejääk
		int j22k = gruppideArv % grupiSuurus;
		System.out.println("Üle jääb " +j22k + " inimest.");
		
	}

}
