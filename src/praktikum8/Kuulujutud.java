package praktikum8;

import praktikum6.Meetodid;

public class Kuulujutud {

	public static void main(String[] args) {

		String[] naised = { "Juta", "Mari", "Kati" };
		String[] mehed = { "Mati", "Lauri", "Mikk" };
		String[] tegevused = { "jalutavad", "flirdivad", "kudrutavad" };

		System.out.format("%s ja %s %s.", 
				suvalineElement(naised), suvalineElement(mehed), suvalineElement(tegevused));

	}

	public static String suvalineElement(String[] s6nad) {
		int suvalineIndeks = Meetodid.suvalineArv(0, s6nad.length - 1);
		return s6nad[suvalineIndeks];
	}

}
