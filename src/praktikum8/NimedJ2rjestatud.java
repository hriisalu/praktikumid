package praktikum8;

import java.util.ArrayList;
import java.util.Collections;

import library.TextIO;

public class NimedJ2rjestatud {

	public static void main(String[] args) {

		ArrayList<String> nimed = new ArrayList<String>();

		System.out.println("Palun sisesta nimesid (tühi sisestus lõpetab)");

		while (true) {
			String nimi = TextIO.getlnString();
			nimed.add(nimi);
			if (nimi.equals(" ")) {
				break;
			}
			
		}

		Collections.sort(nimed);
		for (String nimi : nimed) {
		    System.out.println(nimi);
		}
	}

}