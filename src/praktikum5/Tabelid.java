package praktikum5;

import library.TextIO;

public class Tabelid {

	public static void main(String[] args) {
		
		System.out.println("Palun sisesta tabeli suurus.");
		int arv = TextIO.getInt();
		
		for (int i = 0; i < arv; i++){
			for (int j = 0; j < arv; j++)  {
				if (j == i) {
					System.out.print("1 ");
				} else {
					System.out.print("0 ");
				}
				
			}
			System.out.println();
		}

		
	}

}
