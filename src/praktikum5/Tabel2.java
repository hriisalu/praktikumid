package praktikum5;

import library.TextIO;

public class Tabel2 {

	public static void main(String[] args) {

		System.out.println("Palun sisesta tabeli suurus.");
		int arv = TextIO.getInt();

		for (int k = 0; k < arv; k++) {
			System.out.print("--");
		}
		
		System.out.println();
		for (int i = 0; i < arv; i++) {
			System.out.print("|");
			for (int j = 0; j < arv; j++) {
	
				if (j == i || j + i + 1 == arv) {
				
					System.out.print("x ");
				} else {
					System.out.print("0 ");
					
				}

			}System.out.print("|");
			System.out.println();
		}

		for (int k = 0; k < arv; k++) {
			System.out.print("--");
		}
	}

}
