package praktikum4;

import library.TextIO;

public class CumLaudeDetektor {

	public static void main(String[] args) {
		System.out.println("Mis on teie diplomitöö hinne?");
		int l6put88 = TextIO.getlnInt();
		
		if (l6put88 < 0 || l6put88 > 5){
			System.out.println("Vigane sisestus!");
		return;} // väljab meetodist
		
		System.out.println("Mis on teie keskmine hinne?");
		double keskmine = TextIO.getlnDouble();

		// System.out.println(keskmine);

		if (keskmine >= 4.5 && l6put88 == 5)
			System.out.println("Jah, saad Cum Laude!");

		else 
			System.out.println("Ei saa cum laudet!");

	}

}
